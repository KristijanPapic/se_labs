from django.shortcuts import render, get_object_or_404
from django.http import JsonResponse, HttpResponseRedirect
from django.urls import reverse

# Create your views here.
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.urls import reverse_lazy
from django.views import generic

from app.models import Image, Comment, Vote, Like

class SignUpView(generic.CreateView):
    form_class = UserCreationForm
    success_url = reverse_lazy('login')
    template_name = 'registration/signup.html'

def profile_page(request, account_id):
    selected_user = get_object_or_404(User,pk=account_id)
    images = Image.objects.filter(user=selected_user)
    if request.user.is_superuser:
        comments = selected_user.comment_set.all()
    else:
        comments = selected_user.comment_set.filter(approved = True)
    context = {'selected_user': selected_user,
    'images': images,
    'comments': comments,
    }
    return render(request, 'app/profile_page.html', context)
