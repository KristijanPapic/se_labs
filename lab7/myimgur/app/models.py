from django.db import models
<<<<<<< HEAD
from django.db.models import constraints
from django.db.models.deletion import CASCADE
from django.utils import timezone
from django.contrib.auth.models import User
=======
from django.utils import timezone
>>>>>>> upstream/master

# Create your models here.
class TimeStamped(models.Model):
    created_at = models.DateTimeField(editable=False)
    updated_at = models.DateTimeField(editable=False)

    def save(self, *args, **kwargs):
        if not self.created_at:
            self.created_at = timezone.now()

        self.updated_at = timezone.now()
        return super(TimeStamped, self).save(*args, **kwargs)

    class Meta:
        abstract = True


class Image(TimeStamped):
<<<<<<< HEAD
    user = models.ForeignKey(User, on_delete=CASCADE)
=======
>>>>>>> upstream/master
    title = models.CharField(max_length=128, unique=True, blank=False)
    url = models.CharField(max_length=512)
    description = models.TextField(blank=True)
    pub_date = models.DateTimeField('Published at')
    upvotes = models.IntegerField(default=0)
    downvotes = models.IntegerField(default=0)

    def __str__(self):
        return self.title

<<<<<<< HEAD
    def author(self):
        return self.user.username

    def user_comments_count(self):
        return self.comment_set.filter(approved=True).count()

    def admin_comments_count(self):
        return self.comment_set.count()

    def votes_score(self):
        upvotes = self.vote_set.filter(upvote=True).count()
        downvotes = self.vote_set.filter(upvote=False).count()
        return upvotes - downvotes
    
    def vote_by(self, user):
        if user.is_authenticated:
            return Vote.objects.filter(user=user, image=self).first()
        else:
            return None
=======
    def comments_count(self):
        return image.comment_set.count()

    def votes(self):
        return self.upvotes - self.downvotes
>>>>>>> upstream/master

class Comment(TimeStamped):
    image = models.ForeignKey(Image, on_delete=models.CASCADE)
    text = models.TextField(blank=False)
<<<<<<< HEAD
    user = models.ForeignKey(User, on_delete=CASCADE)
    approved = models.BooleanField(null=False, default=False)
=======
    author = models.CharField(max_length=256, blank=False)
>>>>>>> upstream/master

    def __str__(self):
        return self.text[:80]

<<<<<<< HEAD
    def author(self):
        return self.user.username

    def likes_count(self):
        return self.like_set.count()
    
    def comment_by(self, user):
        if user.is_authenticated:
            return Like.objects.filter(user=user, comment=self).first()
        else:
            return None

class Like(TimeStamped):
    comment = models.ForeignKey(Comment, on_delete=CASCADE)
    user = models.ForeignKey(User, on_delete=CASCADE)

    class Meta:
        constraints = [
            models.UniqueConstraint(name='user_like', fields=['comment', 'user'])
        ]


class Vote(TimeStamped):
    image = models.ForeignKey(Image, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    upvote = models.BooleanField(null=False, default=True)

    class Meta:
        constraints = [
            models.UniqueConstraint(name='user_vote', fields=['image', 'user'])
        ]
    def __str__(self):
        return f"{self.user.username} voted on {self.image.title}"

    def downvote(self):
        return not self.upvote
=======
>>>>>>> upstream/master
    


        
