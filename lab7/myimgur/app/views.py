from django.shortcuts import render, get_object_or_404
from django.http import JsonResponse, HttpResponseRedirect
from django.urls import reverse
<<<<<<< HEAD

from .models import Image, Comment, Vote, Like
from .forms import ImageForm, CommentForm
=======
from .models import Image, Comment
>>>>>>> upstream/master

# Create your views here.

def index(request):
    images = Image.objects.order_by('-pub_date')
<<<<<<< HEAD
    votes = [image.vote_by(request.user) for image in images]
    context = { 'images_with_votes': zip(images, votes)}
=======
    context = { 'images': images}
>>>>>>> upstream/master
    return render(request, 'app/index.html', context)

def detail(request, image_id):
    image = get_object_or_404(Image,pk=image_id)
<<<<<<< HEAD
    if request.user.is_superuser:
        comments = image.comment_set.all()
    else:
        comments = image.comment_set.filter(approved = True)
    likes=[comment.comment_by(request.user) for comment in comments]

    context = {'image': image, 
                'vote': image.vote_by(request.user),
               'comments_with_likes': zip(comments,likes),
               'form': CommentForm(),
              }
    return render(request, 'app/detail.html', context)

def create_image(request):
    if request.method == 'POST' and request.user.is_authenticated:
        form = ImageForm(request.POST)
        if form.is_valid():
            saved_image = form.save(commit = False)
            saved_image.user = request.user
            saved_image.save()
            return HttpResponseRedirect(reverse('app:detail', args=(saved_image.id,)))
    else:
        form = ImageForm()
    context = {'form': form, 'action': 'create'}
    return render(request, 'app/create_image.html',context)

def update_image(request, image_id):
    image = get_object_or_404(Image, pk=image_id)
    if request.method == 'POST' and (request.user.is_superuser or image.user == request.user):
        form = ImageForm(request.POST, instance=image)
        if form.is_valid():
            saved_image = form.save()
            return HttpResponseRedirect(reverse('app:detail', args=(saved_image.id,)))
    else:
        form = ImageForm(instance=image)
    context = {'form': form, 'action': 'update'}
    return render(request, 'app/create_image.html',context)

def delete_image(request, image_id):
    image = get_object_or_404(Image, pk=image_id)
    if request.method == 'POST' and (request.user.is_superuser or image.user == request.user):
        image.delete()
    return HttpResponseRedirect(reverse('app:index'))

def comment(request, image_id):
    if request.method == 'POST' and request.user.is_authenticated:
        image = get_object_or_404(Image, pk=image_id)
        form = CommentForm(request.POST)
        if form.is_valid():
            comment = form.save(commit=False)
            comment.image = image
            comment.user = request.user
            if request.user.is_superuser:
                comment.approved = True
            comment.save()
            return HttpResponseRedirect(reverse('app:detail',args=(comment.image.id,)))
        else:
            return render(request, 'app/detail.html', {
                'image': image,
                'comments': image.comment_set.all(),
                'form': form,
            })
    else:
        return HttpResponseRedirect(reverse('app:detail', args=(image_id,)))

def approve_comment(request, image_id, comment_id):
    if request.method == 'POST' and request.user.is_superuser:
        comment = get_object_or_404(Comment, pk=comment_id)
        comment.approved = True
        comment.save()
    return HttpResponseRedirect(reverse('app:detail', args=(image_id,)))

def like_comment(request, image_id, comment_id):
    if request.method == 'POST' and request.user.is_authenticated:
        comment = get_object_or_404(Comment, pk=comment_id)
        user = request.user
        like = Like.objects.filter(user=request.user, comment = comment).first()
        if like:
            like.delete()
        else:  
            like = Like(comment = comment,
            user = user)
            try:
                like.full_clean()
                like.save()
            except:
                return None
    return HttpResponseRedirect(reverse('app:detail', args=(image_id,)))

def vote(request, image_id, upvote):
    image= get_object_or_404(Image, pk=image_id)
    vote = Vote.objects.filter(user=request.user, image=image).first()
    if vote:
        if vote.upvote == upvote:
            vote.delete()
            return None
        else:
            vote.upvote = upvote
    else:
        vote = Vote(user=request.user, image=image, upvote=upvote)
    try:
        vote.full_clean()
        vote.save()
    except:
        return None
    else:
        return vote

def upvote(request, image_id):
    if request.method == 'POST' and request.user.is_authenticated:
        vote(request, image_id, True)
    return HttpResponseRedirect(reverse('app:detail', args=(image_id,)))

def downvote(request, image_id):
    if request.method == 'POST' and request.user.is_authenticated:
        vote(request, image_id, False)
    return HttpResponseRedirect(reverse('app:detail', args=(image_id,)))
=======
    context = {'image': image, 
               'comments': image.comment_set.all(),
              }
    return render(request, 'app/detail.html', context)

def comment(request, image_id):
    image = get_object_or_404(Image, pk=image_id)
    try:
        comment = image.comment_set.create(
                   author=request.POST['author'],
                   text=request.POST['comment'],
                )
    except (KeyError, Comment.DoesNotExist):
        # Redisplay the comment posting form.
        return render(request, 'app/detail.html', {
            'image': image,
            'error_message': "Posting failed!",
        })
    else:
        # Always return an HttpResponseRedirect after successfully dealing
        # with POST data. This prevents data from being posted twice if a
        # user hits the Back button.
        return HttpResponseRedirect(reverse('app:detail', args=(image.id,)))
>>>>>>> upstream/master

