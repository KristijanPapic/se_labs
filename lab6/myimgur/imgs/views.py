<<<<<<< HEAD
from django.http.response import HttpResponseRedirect
from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse
from .models import Image, Comment
from django.urls import reverse
=======
from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse
from .models import Image, Comment
>>>>>>> upstream/master

# Create your views here.
def index(request):
    images = Image.objects.order_by('-pub_date')
    context = {
        'all_images': images
    }
    return render(request, 'imgs/index.html', context)

def detail(request, image_id):
    image = get_object_or_404(Image,pk=image_id)
    context = {
        'image': image,
        'comments': image.comment_set.all()
    }
    return render(request, 'imgs/detail.html', context)

<<<<<<< HEAD
def post_comment(request, image_id):
    image = get_object_or_404(Image,pk=image_id)
    comment = Comment(nick=request.POST['nick'],
    text=request.POST['text'],
    image=image)
    comment.save()
    return HttpResponseRedirect(reverse('detail',args=(image.id,)))

def post_image(request):
    image = Image(title=request.POST['title'],
    url=request.POST['url'],
    description=request.POST['description'],
    pub_date=request.POST['pub_date'])
    image.save()
    return HttpResponseRedirect("/")

=======
>>>>>>> upstream/master
def about(request):
    context = {}
    return render(request, 'imgs/about.html', context)
