mkdir lab5
cd lab5/
ls
python -m venv django             (postavi virtual env django)
source django/Scripts/activate    (aktiviraj virtu env)
pip install django                (instaliraj django u virtu env)
django-admin startproject myimgur (postavi projekt)
ls
pip freeze > requirements.txt 
cat requirements.txt
cd ..
git status
git add lab5/  (dodaj django u gitignore)
git commit -m "added lab 5"