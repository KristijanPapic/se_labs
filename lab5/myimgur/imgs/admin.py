from django.contrib import admin
from .models import Image, Comment
# Register your models here.
class ImageAdmin(admin.ModelAdmin):
    fields = ['title', 'pub_date', 'url']

admin.site.register(Image, ImageAdmin)
admin.site.register(Comment)