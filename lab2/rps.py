def is_valid(choice):
    return choice in ["rock", "paper", "scissors","lizard","spock"]

def check_input(player1, player2):
    if not is_valid(player1):
        return False
    if not is_valid(player2):
        return False
    return True

def test_valid_choice(choice, expected):
    try:
        result = is_valid(choice)
        assert result == expected
    except:
        print(choice)
        print(expected)
        raise
    else:
        print(".")

def winner(player1, player2):
    if not check_input(player1, player2):
        return False
    if player1 == player2:
        return("draw")
    elif player1 == "rock":
        if player2 == "paper":
            return("player2")
        elif player2 == "scissors":
            return("player1")
        elif player2 == "lizard":
            return("player1")
        elif player2 == "spock":
            return("player2")
        elif player2 == "rock":
            return("draw")

    elif player1 == "paper":
        if player2 == "rock":
            return("player1")
        elif player2 == "scissors":
            return("player2")
        elif player2 == "lizard":
            return("player2")
        elif player2 == "spock":
            return("player1")
        elif player2 == "paper":
            return("draw")
            
    elif player1 == "scissors":
        if player2 == "rock":
            return("player2")
        elif player2 == "paper":
            return("player1")
        elif player2 == "lizard":
            return("player1")
        elif player2 == "spock":
            return("player2")
        elif player2 == "scissors":
            return("draw")

    elif player1 == "lizard":
        if player2 == "rock":
            return("player2")
        elif player2 == "paper":
            return("player1")
        elif player2 == "scissors":
            return("player2")
        elif player2 == "spock":
            return("player1")
        elif player2 == "lizard":
            return("draw")

    elif player1 == "spock":
        if player2 == "rock":
            return("player1")
        elif player2 == "paper":
            return("player2")
        elif player2 == "lizard":
            return("player2")
        elif player2 == "scissors":
            return("player1")
        elif player2 == "spock":
            return("draw")
    return False

def threeWayWinner(player1,player2,player3):
    p1points=0
    p2points=0
    p3points=0
    rez=winner(player1,player2)
    if rez == "player1":
        p1points += 1
    elif rez == "player2":
        p2points += 1
    
    rez=winner(player1,player3)
    if rez == "player1":
        p1points += 1
    elif rez == "player2":
        p3points += 1

    rez=winner(player2,player3)
    if rez == "player1":
        p2points += 1
    elif rez == "player2":
        p3points += 1

    if (p1points == p2points and p1points >= p3points) or (p1points == p3points and p1points >= p2points) or (p2points == p3points and p2points >= p1points):
        return("draw")
    dict = {p1points:"player1",p2points:"player2",p3points:"player3"}
    return dict.get(max(dict))
    


def main():
    while True:
        player1 = input("rock-paper-scissors: ")
        player2 = input("rock-paper-scissors: ")
        player3 = input("rps")
        
        nastavi = input("nastavi yes/no")
        if nastavi == "no":
            break
def test_winner(player1, player2, expected):
    try:
        result = winner(player1, player2)
        assert result == expected
    except:
        print(player1, "vs.", player2)
        print("result: ", result)
        print("expected: ", expected)
        raise
    else:
        print(".")

def test_3winner(player1, player2,player3, expected):
    try:
        result = threeWayWinner(player1, player2, player3)
        assert result == expected
    except:
        print(player1, " vs. ", player2, " vs. ", player3)
        print("result: ", result)
        print("expected: ", expected)
        raise
    else:
        print(".")

def unit_tests():
    """
    test_valid_choice("rock",True)
    test_valid_choice("paper",True)
    test_valid_choice("scissors",True)
    test_valid_choice("lizard",True)
    test_valid_choice("spock",True)
    test_valid_choice("pero",False)

    test_winner("rock", "rock", "draw")
    test_winner("rock", "paper", "player2")
    test_winner("rock", "scissors", "player1")
    test_winner("rock", "spock", "player2")
    test_winner("rock", "lizard", "player1")

    test_winner("paper", "paper", "draw")
    test_winner("paper", "rock", "player1")
    test_winner("paper", "scissors", "player2")
    test_winner("paper", "spock", "player1")
    test_winner("paper", "lizard", "player2")

    test_winner("scissors", "scissors", "draw")
    test_winner("scissors", "rock", "player2")
    test_winner("scissors", "paper", "player1")
    test_winner("scissors", "spock", "player2")
    test_winner("scissors", "lizard", "player1")


    test_winner("lizard", "rock", "player2")
    test_winner("lizard", "paper", "player1")
    test_winner("lizard", "scissors", "player2")
    test_winner("lizard", "spock", "player1")
    test_winner("lizard", "lizard", "draw")

    test_winner("spock", "rock", "player1")
    test_winner("spock", "paper", "player2")
    test_winner("spock", "scissors", "player1")
    test_winner("spock", "lizard", "player2")
    test_winner("spock", "spock", "draw")
    """
    test_3winner("rock", "paper", "lizard", "draw")

if __name__ == "__main__":
    #main()
    unit_tests()
